#include "ExtendedDocument.h"
#include "MemberNotFoundException.h"

bool ExtendedDocument::HasStringMember(const char* name) const {
    auto iterator = FindMember(name);
    return iterator != MemberEnd() && iterator->value.IsString();
}

bool ExtendedDocument::HasUint64Member(const char* name) const {
    auto iterator = FindMember(name);
    return iterator != MemberEnd() && iterator->value.IsUint64();
}

bool ExtendedDocument::HasArrayMember(const char* name) const {
    auto iterator = FindMember(name);
    return iterator != MemberEnd() && iterator->value.IsArray();
}

const char* ExtendedDocument::GetStringMember(const char* name) const {
    auto iterator = FindMember(name);
    if (iterator == MemberEnd() || !iterator->value.IsString()) throw MemberNotFoundException("String", name);
    return iterator->value.GetString();
}

uint64_t ExtendedDocument::GetUint64Member(const char* name) const {
    auto iterator = FindMember(name);
    if (iterator == MemberEnd() || !iterator->value.IsUint64()) throw MemberNotFoundException("Uint64", name);
    return iterator->value.GetUint64();
}

rapidjson::Document::ConstArray ExtendedDocument::GetArrayMember(const char* name) const {
    auto iterator = FindMember(name);
    if (iterator == MemberEnd() || !iterator->value.IsArray()) throw MemberNotFoundException("Array", name);
    return iterator->value.GetArray();
}