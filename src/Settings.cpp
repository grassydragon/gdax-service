#include "Settings.h"
#include "ExtendedDocument.h"
#include "MemberNotFoundException.h"
#include <iostream>
#include <rapidjson/error/en.h>
#include <rapidjson/filereadstream.h>

using namespace std;
using  namespace rapidjson;

bool Settings::load(const char* path) {
    FILE* file = fopen(path, "r");

    if (file == nullptr) {
        cout << "Settings file opening error: " << strerror(errno) << endl;
        cout << "Path: " << path << endl;
        return false;
    }

    char buffer[BUFFER_SIZE];

    FileReadStream stream(file, buffer, BUFFER_SIZE);

    ExtendedDocument settings;

    settings.ParseStream(stream);

    if (settings.HasParseError()) {
        cout << "Settings file parse error: " << GetParseError_En(settings.GetParseError()) << endl;
        cout << "Error offset: " << settings.GetErrorOffset();
        fclose(file);
        return false;
    }

    if (!settings.IsObject()) {
        cout << "Settings file isn't Json object" << endl;
        fclose(file);
        return false;
    }

    try {
        httpEndpoint = settings.GetStringMember("httpEndpoint");
        wsEndpoint = settings.GetStringMember("wsEndpoint");

        Document::ConstArray array = settings.GetArrayMember("productIds");

        if (array.Empty()) {
            cout << "Settings file has empty \"productIds\" array" << endl;
            fclose(file);
            return false;
        }

        productIds.reserve(array.Size());

        for (const Value& element : array) {
            if (!element.IsString()) {
                cout << "Settings file has element in \"productIds\" array that isn't string" << endl;
                fclose(file);
                return false;
            }

            productIds.emplace_back(element.GetString());
        }
    }
    catch (const MemberNotFoundException& e) {
        cout << "Incorrect settings file format: " << e.what() << endl;
        fclose(file);
        return false;
    }

    fclose(file);

    return true;
}

const std::string& Settings::getHttpEndpoint() const {
    return httpEndpoint;
}

const std::string& Settings::getWsEndpoint() const {
    return wsEndpoint;
}

const std::vector<std::string>& Settings::getProductIds() const {
    return productIds;
}
