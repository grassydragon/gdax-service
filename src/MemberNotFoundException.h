#ifndef GDAX_SERVICE_MEMBERNOTFOUNDEXCEPTION_H
#define GDAX_SERVICE_MEMBERNOTFOUNDEXCEPTION_H

#include <exception>
#include <string>

/**
 * Иключение, которое создаётся, если поле не было найдено
 */
class MemberNotFoundException : public std::exception {

private:

    /**
     * Сообщение исключения
     */
    std::string message;

public:

    /**
     * Конструктор, который создаёт сообщение с указанием на тип и имя поля
     * @param type тип поля
     * @param name имя поля
     */
    explicit MemberNotFoundException(const char* type, const char* name);

    const char* what() const noexcept override;

};

#endif //GDAX_SERVICE_MEMBERNOTFOUNDEXCEPTION_H