#ifndef GDAX_SERVICE_ORDERBOOK_H
#define GDAX_SERVICE_ORDERBOOK_H

#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <string>
#include <tuple>

namespace mp = boost::multiprecision;
namespace mi = boost::multi_index;

/**
 * Тип чисел с фиксированной точностью, который используется для представления дробных значений
 */
typedef mp::number<mp::cpp_dec_float<10>> number_type;

/**
 * Тип заявки
 */
enum OrderType { BID = 0, ASK = 1 };

/**
 * Стакан
 */
class OrderBook {

private:

    /**
     * Заявка
     */
    class Order {

    public:

        /**
         * Цена
         */
        number_type price;

        /**
         * Размер
         */
        number_type size;

        /**
         * Идентификатор
         */
        std::string id;

    public:

        /**
         * Конструктор, который копирует значения всех параметров
         * @param price цена
         * @param size размер
         * @param id идентификатор
         */
        Order(const number_type& price, const number_type& size, const std::string& id) : price(price), size(size), id(id) {

        }

    };

private:

    /**
     * Массив контейнеров, которые обеспечивают доступ к заявкам по идентификатору и сортировку заявок по цене, для хранения двух типов заявок
     */
    boost::multi_index_container<
            Order,
            mi::indexed_by<
                    mi::hashed_unique<mi::member<Order, std::string, &Order::id>>,
                    mi::ordered_non_unique<mi::member<Order, number_type, &Order::price>>
            >
    > orders[2];

public:

    /**
     * Добавляет заявку, если заявка с таким идентификатором и типом не существует
     * @param price цена
     * @param size размер
     * @param id идентификатор
     * @param type тип
     */
    void addOrder(const number_type& price, const number_type& size, const std::string& id, OrderType type);

    /**
     * Удаляет заявку, если заявка с таким идентификатором и типом существует
     * @param id идентификатор
     * @param type тип
     */
    void removeOrder(const std::string& id, OrderType type);

    /**
     * Изменяет размер заявки, если заявка с таким идентификатором и типом существует
     * @param size размер
     * @param id идентификатор
     * @param type тип
     */
    void changeOrderSize(const number_type& size, const std::string& id, OrderType type);

    /**
     * Уменьшает размер заявки, если заявка с таким идентификатором и типом существует
     * @param change изменение размера
     * @param id идентификатор
     * @param type тип
     */
    void decreaseOrderSize(const number_type& change, const std::string& id, OrderType type);

    /**
     * Получает лучшую заявку
     * @param type тип
     * @return цена, общий размер, количество лучших заявок или 0, 0, 0, если заявки с таким типом не существуют
     */
    std::tuple<number_type, number_type, size_t> getBestOrder(OrderType type);

    /**
     * Удаляет все заявки
     */
    void clear();

};

#endif //GDAX_SERVICE_ORDERBOOK_H