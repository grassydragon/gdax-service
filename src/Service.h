#ifndef GDAX_SERVICE_SERVICE_H
#define GDAX_SERVICE_SERVICE_H

#include "Settings.h"
#include "OrderBook.h"
#include "ExtendedDocument.h"
#include <libwebsockets.h>
#include <unordered_map>
#include <unordered_set>

/**
 * Сервис
 */
class Service {

private:

    /**
     * Пара валют
     */
    class Product {

    public:

        /**
         * Идентификатор
         */
        std::string id;

        /**
        * Стакан
        */
        OrderBook orderBook;

        /**
        * Порядковый номер
        */
        uint64_t sequence = 0;

        /**
         * Цены лучших заявок
         */
        number_type prices[2];

        /**
         * Общие размеры лучших заявок
         */
        number_type sizes[2];

        /**
         * Количества лучших заявок
         */
        size_t numbers[2] = { 0, 0 };

        /**
         * Время изменения лучших заявок
         */
        std::string times[2];

        /**
         * Цена последней транзакции
         */
        number_type matchPrice;

        /**
         * Размер последней транзакции
         */
        number_type matchSize;

        /**
         * Количество одинаковых последних транзакций
         */
        size_t matchNumber = 0;

        /**
         * Время последней транзакции
         */
        std::string matchTime;

        /**
         * Время последнего обработанного сообщения
         */
        std::string time;

        /**
        * Показывает, загружены ли данные в стакан
        */
        bool orderBookLoaded = false;

    public:

        /**
         * Конструктор
         * @param id идентификатор
         */
        explicit Product(const std::string& id);

        /**
         * Возвращает объект к первоначальному состоянию
         */
        void reset();

        /**
        * Применяет сообщение в формате Json к стакану
        * @param message сообщение в формате Json
        * @return true, если сообщение было применено, false, если сообщение имеет неверный порядковый номер или произошла ошибка
        */
        bool applyMessage(const ExtendedDocument& message);

        /**
         * Обновляет данные о лучших заявках
         * @return true, если данные были обновлены, false, если нет
         */
        bool updateData();

        /**
         * Выводит данные о лучших заявках и последней транзакции
         */
        void outputData();


    };

private:

    /**
     * Размер части строки HTTP ответа от сервера
     */
    static const int PART_SIZE = 1024;

    /**
     * Массив протоколов
     */
    const lws_protocols PROTOCOLS[2] = { { "default", callback, 0, 0, 0, nullptr, 0 }, { nullptr, nullptr, 0, 0, 0, nullptr, 0 } };

    /**
     * Типы сообщений для WebSocket соединения
     */
    const std::unordered_set<std::string> MESSAGE_TYPES = { "error", "subscriptions", "received", "open", "done", "match", "change", "activate" };

private:

    /**
     * Настройки
     */
    const Settings& settings;

    /**
     * Хеш-таблица пар валют
     */
    std::unordered_map<std::string, Product> products;

    /**
     * Итератор для хеш-таблицы пар валют
     */
    std::unordered_map<std::string, Product>::iterator productsIterator;

    /**
     * Список частей строки HTTP ответа от сервера
     */
    std::list<std::vector<char>> parts;

    /**
     * Часть строки HTTP ответа от сервера
     */
    std::vector<char> part;

    /**
     * Список сообщений в формате Json
     */
    std::list<ExtendedDocument> messages;

    /*
     * Указатель на контекст
     */
    lws_context* context = nullptr;

    /*
     * Указатель на HTTP соединение
     */
    lws* http = nullptr;

    /*
     * Указатель на WebSocket соединение
     */
    lws* ws = nullptr;

    /**
     * Код HTTP ответа от сервера
     */
    unsigned int httpResponseCode = 0;

    /**
     * Показывает, является ли сервис активным
     */
    bool active = false;

    /**
     * Показывает, подписан ли сервис на сообщения по WebSocket соединению
     */
    bool subscribed = false;

public:

    explicit Service(const Settings& settings);

    /**
     * Запускает сервис
     */
    void start();

    /**
     * Останавливает сервис
     */
    void stop();

private:

    /**
     * Создаёт контекст
     */
    void createContext();

    /**
     * Создаёт HTTP соединение
     */
    void connectToHttpServer(const std::string& productId);

    /**
     * Создаёт WebSocket соединение
     */
    void connectToWsServer();

    /**
     * Загружает данные в стакан
     */
    void loadOrderBook();

    /**
     * Обработчик событий для HTTP и WebSocket соединений
     */
    static int callback(lws* wsi, enum lws_callback_reasons reason, void* user, void* in, size_t len);

};

#endif //GDAX_SERVICE_SERVICE_H