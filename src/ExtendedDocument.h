#ifndef GDAX_SERVICE_EXTENDEDDOCUMENT_H
#define GDAX_SERVICE_EXTENDEDDOCUMENT_H

#include <rapidjson/document.h>

/**
 * Расширение класса rapidjson::Document
 */
class ExtendedDocument : public rapidjson::Document {

public:

    /**
     * Проверяет наличия поля с типом String
     * @param name имя поля
     * @return true, если поле существует и имеет тип String, false, если нет
     */
    bool HasStringMember(const char* name) const;

    /**
     * Проверяет наличие поля с типом Uint64
     * @param name имя поля
     * @return true, если поле существует и имеет тип Uint64, false, если нет
     */
    bool HasUint64Member(const char* name) const;

    /**
     * Проверяет наличие поля с типом Array
     * @param name имя поля
     * @return true, если поле существует и имеет тип Array, false, если нет
     */
    bool HasArrayMember(const char* name) const;

    /**
     * Получает поле с типом String, создаёт исключение, если поле с таким типом и именем не существует
     * @param name имя поля
     * @return значение поля
     */
    const char* GetStringMember(const char* name) const;

    /**
     * Получает поле с типом Uint64, создаёт исключение, если поле с таким типом и именем не существует
     * @param name имя поля
     * @return значение поля
     */
    uint64_t GetUint64Member(const char* name) const;

    /**
     * Получает поле с типом Array, создаёт исключение, если поле с таким типом и именем не существует
     * @param name имя поля
     * @return значение поля
     */
    rapidjson::Document::ConstArray GetArrayMember(const char* name) const;

};

#endif //GDAX_SERVICE_MESSAGE_H