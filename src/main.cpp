#include "Service.h"
#include "Settings.h"
#include <csignal>
#include <iostream>

using namespace std;

Settings* settings = nullptr;

Service* service = nullptr;

void signalHandler(int signalNumber) {
    service->stop();
    signal(SIGINT, SIG_IGN);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        cout << "Usage: gdax-service path" << endl;
        cout << "path - path to Json settings file" << endl;
        cout << "Required fields: " << endl;
        cout << "httpEndpoint - HTTP server address (string)" << endl;
        cout << "wsEndpoint - WebSocket server address (string)" << endl;
        cout << "productIds - product ids to subscribe to (array of strings)" << endl;
        return 0;
    }

    settings = new Settings();

    if (!settings->load(argv[1])) return 0;

    service = new Service(*settings);

    signal(SIGINT, signalHandler);

    service->start();

    delete settings;
    delete service;

    return 0;
}