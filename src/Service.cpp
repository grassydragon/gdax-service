#include "Service.h"
#include "PartStream.h"
#include "HttpReaderHandler.h"
#include "MemberNotFoundException.h"
#include <rapidjson/error/en.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>
#include <iostream>

using namespace std;
using namespace rapidjson;

Service::Product::Product(const std::string& id) : id(id) {

}

void Service::Product::reset()  {
    orderBook.clear();
    matchPrice = 0;
    matchSize = 0;
    matchNumber = 0;
    matchTime.clear();
    orderBookLoaded = false;
}

bool Service::Product::applyMessage(const ExtendedDocument& message) {
    bool output = false;

    if (message["sequence"].GetUint64() != sequence + 1) return false;

    const char* messageType = message["type"].GetString();

    try {
        OrderType orderType = (strcmp(message.GetStringMember("side"), "buy") == 0 ? BID : ASK);

        time = message.GetStringMember("time");

        if (strcmp(messageType, "open") == 0) {
            number_type price(message.GetStringMember("price"));
            number_type size(message.GetStringMember("remaining_size"));
            string id(message.GetStringMember("order_id"));

            orderBook.addOrder(price, size, id, orderType);
        }
        else if (strcmp(messageType, "done") == 0) {

            string id(message.GetStringMember("order_id"));

            orderBook.removeOrder(id, orderType);
        }
        else if (strcmp(messageType, "match") == 0) {
            number_type change(message.GetStringMember("size"));
            string id(message.GetStringMember("maker_order_id"));

            orderBook.decreaseOrderSize(change, id, orderType);

            number_type tmpMatchPrice(message.GetStringMember("price"));
            number_type tmpMatchSize(message.GetStringMember("size"));

            if (tmpMatchPrice != matchPrice || tmpMatchSize != matchSize) matchNumber = 0;

            matchPrice = tmpMatchPrice;
            matchSize = tmpMatchSize;
            ++matchNumber;
            matchTime = time;

            output = true;
        }
        else if (strcmp(messageType, "change") == 0 && message.HasMember("new_size")) {
            number_type size(message.GetStringMember("new_size"));
            string id(message.GetStringMember("order_id"));

            orderBook.changeOrderSize(size, id, orderType);
        }
    }
    catch (const MemberNotFoundException& e) {
        cout << "Incorrect WebSocket message format for type \"" << messageType << "\":" << e.what() << endl;
        cout << "Message: " << endl;

        OStreamWrapper wrapper(cout);
        Writer<OStreamWrapper> writer(wrapper);
        message.Accept(writer);

        return false;
    }

    ++sequence;

    output |= updateData();

    if (output) outputData();

    return true;
}

bool Service::Product::updateData() {
    bool updated = false;

    OrderType types[2] = { BID, ASK };

    number_type tmpPrices[2];
    number_type tmpSizes[2];
    size_t tmpNumbers[2];

    for (OrderType type : types) {
        tie(tmpPrices[type], tmpSizes[type], tmpNumbers[type]) = orderBook.getBestOrder(type);

        if (tmpPrices[type] != prices[type]) {
            prices[type] = tmpPrices[type];
            times[type] = time;
            updated = true;
        }

        if (tmpSizes[type] != sizes[type]) {
            sizes[type] = tmpSizes[type];
            times[type] = time;
            updated = true;
        }

        if (tmpNumbers[type] != numbers[type]) {
            numbers[type] = tmpNumbers[type];
            times[type] = time;
            updated = true;
        }
    }

    return updated;
}

void Service::Product::outputData() {
    cout << "{ \"product\": \"" << id << "\", ";
    cout << "\"sequence\": " << sequence << ", ";
    cout << "\"bids\": [ \"" << prices[BID] << "\", \"" << sizes[BID] << "\", " << numbers[BID] << ", \"" << times[BID] << "\" ], ";
    cout << "\"asks\": [ \"" << prices[ASK] << "\", \"" << sizes[ASK] << "\", " << numbers[ASK] << ", \"" << times[ASK] << "\" ], ";
    cout << "\"last\": [ \"" << matchPrice << "\", \"" << matchSize << "\", " << matchNumber << ", \"" << matchTime << "\" ], ";
    cout << "\"time\": \"" << time << "\" }";
    cout << endl;
}

Service::Service(const Settings& settings) : settings(settings) {
    for (const string& id : settings.getProductIds()) {
        products.emplace(id, Product(id));
    }

    productsIterator = products.end();
}

void Service::start() {
    cout << "Starting service" << endl;

    createContext();

    if (context == nullptr) {
        cout << "Context creation error" << endl;
        return;
    }

    active = true;

    while (active) {
        if (productsIterator == products.end()) {
            productsIterator = products.begin();

            while (productsIterator != products.end() && productsIterator->second.orderBookLoaded) ++productsIterator;
        }

        if (ws == nullptr) connectToWsServer();

        if (productsIterator != products.end() && http == nullptr && !messages.empty()) connectToHttpServer(productsIterator->second.id);

        lws_service(context, 1000);
    }

    http = nullptr;
    ws = nullptr;

    lws_context_destroy(context);

    cout << "Stopping service" << endl;
}

void Service::stop() {
    active = false;
    lws_cancel_service(context);
}

void Service::createContext() {
    lws_context_creation_info info {};

    info.options = LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
    info.port = CONTEXT_PORT_NO_LISTEN;
    info.protocols = PROTOCOLS;
    info.ws_ping_pong_interval = 10;
    info.timeout_secs = 5;
    info.ka_time = 20;
    info.ka_probes = 1;
    info.ka_interval = 1;

    context = lws_create_context(&info);
}

void Service::connectToHttpServer(const std::string& productId) {
    string path = "/products/" + productId + "/book?level=3";

    lws_client_connect_info info {};

    info.context = context;
    info.ssl_connection = LCCSCF_USE_SSL;
    info.port = 443;
    info.address = settings.getHttpEndpoint().c_str();
    info.path = path.c_str();
    info.host = info.address;
    info.origin = info.address;
    info.method = "GET";
    info.protocol = PROTOCOLS[0].name;
    info.pwsi = &http;
    info.userdata = this;
    info.parent_wsi = ws;

    lws_client_connect_via_info(&info);
}

void Service::connectToWsServer() {
    lws_client_connect_info info {};
    
    info.context = context;
    info.ssl_connection = LCCSCF_USE_SSL;
    info.port = 443;
    info.address = settings.getWsEndpoint().c_str();
    info.path = "/";
    info.host = info.address;
    info.origin = info.address;
    info.protocol = PROTOCOLS[0].name;
    info.pwsi = &ws;
    info.userdata = this;

    lws_client_connect_via_info(&info);
}

void Service::loadOrderBook() {
    Product& product = productsIterator->second;

    PartStream stream(parts);
    HttpReaderHandler handler(product.orderBook);
    Reader reader;

    reader.Parse<kParseNumbersAsStringsFlag>(stream, handler);

    if (reader.HasParseError()) {
        cout << "HTTP response parse error: " << GetParseError_En(reader.GetParseErrorCode()) << endl;
        cout << "Error offset: " << reader.GetErrorOffset() << endl;
        cout << "Server response: " << endl;

        stream.reset();
        while (stream.Peek() != '\0') cout << stream.Take();
        cout << endl;

        return;
    }

    product.sequence = handler.getSequence();

    auto iterator = messages.begin();

    while (iterator != messages.end() && (*iterator)["sequence"].GetUint64() <= product.sequence) ++iterator;

    while (iterator != messages.end()) {
        if (!product.applyMessage(*iterator)) break;
        ++iterator;
    }

    if (iterator == messages.end()) {
        product.orderBookLoaded = true;
        productsIterator = products.end();
    }
    else {
        product.reset();
    }
}

int Service::callback(lws* wsi, enum lws_callback_reasons reason, void* user, void* in, size_t len) {
    Service& service = *static_cast<Service*>(user);

    switch (reason) {
        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: {
            if (wsi == service.http) {
                cout << "HTTP connection error: " << (in != nullptr ? static_cast<char*>(in) : "undefined") << endl;
                service.messages.clear();
                service.http = nullptr;
            }
            else {
                cout << "WebSocket connection error: " << (in != nullptr ? static_cast<char*>(in) : "undefined") << endl;
                service.ws = nullptr;
            }

            break;
        }

        case LWS_CALLBACK_CLIENT_APPEND_HANDSHAKE_HEADER: {
            const char header[] = "User-Agent: libwebsockets\x0d\x0a";

            char** p = static_cast<char**>(in);

            if (len < sizeof(header)) {
                cout << "HTTP header append failed" << endl;
                return -1;
            }

            *p += sprintf(*p, header);

            break;
        }

        //События для HTTP соединения

        case LWS_CALLBACK_ESTABLISHED_CLIENT_HTTP: {
            service.httpResponseCode = lws_http_client_http_response(wsi);

            break;
        }

        case LWS_CALLBACK_RECEIVE_CLIENT_HTTP: {
            service.part.resize(PART_SIZE);

            char* buffer = service.part.data();

            int length = PART_SIZE;

            if (lws_http_client_read(wsi, &buffer, &length) < 0) {
                if (service.http != nullptr) cout << "HTTP read failed" << endl;
                return -1;
            }

            break;
        }

        case LWS_CALLBACK_RECEIVE_CLIENT_HTTP_READ: {
            service.part.resize(len);
            service.parts.emplace_back(move(service.part));

            break;
        }

        case LWS_CALLBACK_COMPLETED_CLIENT_HTTP: {
            if (service.httpResponseCode != HTTP_STATUS_OK) {
                cout << "Incorrect HTTP response code: " << service.httpResponseCode << endl;
                cout << "Server response: " << endl;

                PartStream stream(service.parts);
                while (stream.Peek() != '\0') cout << stream.Take();
                cout << endl;
            }
            else {
                service.loadOrderBook();
            }

            service.http = nullptr;

            break;
        }

        case LWS_CALLBACK_CLOSED_CLIENT_HTTP: {
            if (service.http != nullptr) {
                cout << "HTTP connection closed" << endl;
                service.http = nullptr;
            }

            service.parts.clear();
            service.part.clear();
            service.messages.clear();

            break;
        }

        //События для WebSocket соединения

        case LWS_CALLBACK_CLIENT_ESTABLISHED: {
            lws_callback_on_writable(wsi);

            break;
        }

        case LWS_CALLBACK_CLIENT_WRITEABLE: {
            if (!service.subscribed) {
                const vector<string>& productIds = service.settings.getProductIds();

                string message = R"({ "type": "subscribe", "product_ids": [ )";

                message += "\"" + productIds[0] + "\"";

                for (int i = 1; i < productIds.size(); ++i) message += ", \"" + productIds[i]  + "\"";

                message += R"(], "channels": [ "full" ] })";

                unsigned char buffer[LWS_PRE + message.size()];

                memcpy(buffer + LWS_PRE, message.c_str(), message.size());

                if (lws_write(wsi, buffer + LWS_PRE, message.size(), LWS_WRITE_TEXT) < message.size()) {
                    cout << "WebSocket write failed" << endl;
                    return -1;
                }

                service.subscribed = true;
            }

            break;
        }

        case LWS_CALLBACK_CLIENT_RECEIVE: {
            if (/*lws_is_first_fragment(wsi) != 1 || Всегда возвращает 0*/ lws_is_final_fragment(wsi) != 1) {
                cout << "WebSocket message fragment received" << endl;
                cout << "Message fragment: " << endl;
                cout << string(static_cast<char*>(in), len) << endl;
                break;
            }

            ExtendedDocument message;

            message.Parse(static_cast<char*>(in), len);

            if (message.HasParseError()) {
                cout << "WebSocket message parse error: " << GetParseError_En(message.GetParseError()) << endl;
                cout << "Error offset: " << message.GetErrorOffset() << endl;
                cout << "Message: " << endl;
                cout << string(static_cast<char*>(in), len) << endl;
                break;
            }

            if (!message.IsObject()) {
                cout << "WebSocket message isn't Json object" << endl;
                cout << "Message:" << endl;
                cout << string(static_cast<char*>(in), len) << endl;
                break;
            }

            if (!message.HasStringMember("type")) {
                cout << "WebSocket message doesn't have \"type\" field" << endl;
                cout << "Message:" << endl;
                cout << string(static_cast<char*>(in), len) << endl;
                break;
            }

            string messageType = message["type"].GetString();

            if (service.MESSAGE_TYPES.count(messageType) == 0) {
                cout << "WebSocket message with unknown type \"" << messageType << "\" received" << endl;
                cout << "Message:" << endl;
                cout << string(static_cast<char*>(in), len) << endl;
                break;
            }

            if (messageType == "error") {
                cout << "WebSocket message with type \"error\" received" << endl;
                cout << "Message:" << endl;
                cout << string(static_cast<char*>(in), len) << endl;
                service.stop();
                break;
            }

            if (message.HasUint64Member("sequence")) {
                if (!message.HasStringMember("product_id")) {
                    cout << "WebSocket message has \"sequence\" field but doesn't have \"product_id\" field" << endl;
                    cout << "Message:" << endl;
                    cout << string(static_cast<char*>(in), len) << endl;
                    break;
                }

                auto iterator = service.products.find(message["product_id"].GetString());

                if (iterator == service.products.end()) {
                    cout << "WebSocket message with unknown \"product_id\" received" << endl;
                    cout << "Message:" << endl;
                    cout << string(static_cast<char*>(in), len) << endl;
                    break;
                }

                Product& product = iterator->second;

                if (product.orderBookLoaded) {
                    if (!product.applyMessage(message)) {
                        product.reset();
                    }
                }
                else if (service.productsIterator != service.products.end() && product.id == service.productsIterator->second.id) {
                    service.messages.emplace_back(move(message));
                }
            }

            break;
        }

        case LWS_CALLBACK_CLOSED: {
            if (service.ws != nullptr) {
                cout << "WebSocket connection closed" << endl;
                service.ws = nullptr;
            }

            for (auto& entry : service.products) entry.second.reset();

            service.productsIterator = service.products.end();

            service.subscribed = false;

            break;
        }

        default:
            break;
    }

    return 0;
}