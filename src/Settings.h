#ifndef GDAX_SERVICE_SETTINGS_H
#define GDAX_SERVICE_SETTINGS_H

#include <string>
#include <vector>

/**
 * Настройки
 */

class Settings {

private:

    /**
     * Размер буфера
     */
    static const size_t BUFFER_SIZE = 1024;

private:

    /**
     * Адрес HTTP сервера
     */
    std::string httpEndpoint;

    /**
     * Адрес WebSocket сервера
     */
    std::string wsEndpoint;

    /**
     * Идентификаторы пар валют
     */
    std::vector<std::string> productIds;

public:

    /**
     * Загружает настройки из файла в формате Json
     * @param path относительный путь
     * @return true, если настройки были загружены, false, если нет
     */
    bool load(const char* path);

    const std::string& getHttpEndpoint() const;

    const std::string& getWsEndpoint() const;

    const std::vector<std::string>& getProductIds() const;

};

#endif //GDAX_SERVICE_SETTINGS_H