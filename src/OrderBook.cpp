#include "OrderBook.h"

void OrderBook::addOrder(const number_type& price, const number_type& size, const std::string& id, OrderType type) {
    orders[type].emplace(price, size, id);
}

void OrderBook::removeOrder(const std::string& id, OrderType type) {
    orders[type].erase(id);
}

void OrderBook::changeOrderSize(const number_type& size, const std::string& id, OrderType type) {
    auto iterator = orders[type].find(id);

    if (iterator != orders[type].end()) {
        Order order = *iterator;
        order.size = size;
        orders[type].replace(iterator, order);
    }
}

void OrderBook::decreaseOrderSize(const number_type& change, const std::string& id, OrderType type) {
    auto iterator = orders[type].find(id);

    if (iterator != orders[type].end()) {
        Order order = *iterator;
        order.size -= change;
        orders[type].replace(iterator, order);
    }
}

std::tuple<number_type, number_type, size_t> OrderBook::getBestOrder(OrderType type) {
    number_type price = 0;
    number_type size = 0;
    size_t number = 0;

    if (!orders[type].empty()) {
        auto& orderedIndex = orders[type].get<1>();

        if (type == BID) {
            auto iterator = orderedIndex.rbegin();

            price = iterator->price;

            do {
                size += iterator->size;
                ++number;
                ++iterator;
            } while (iterator != orderedIndex.rend() && iterator->price == price);
        }
        else {
            auto iterator = orderedIndex.begin();

            price = iterator->price;

            do {
                size += iterator->size;
                ++number;
                ++iterator;
            } while (iterator != orderedIndex.end() && iterator->price == price);
        }
    }

    return std::make_tuple(price, size, number);
}

void OrderBook::clear() {
    orders[BID].clear();
    orders[ASK].clear();
}