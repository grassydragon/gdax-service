#include "PartStream.h"
#include <assert.h>

PartStream::PartStream(const std::list<std::vector<Ch>>& parts) : parts(parts) {
    listIterator = parts.begin();
    skipEmptyParts();
}

PartStream::Ch PartStream::Peek() const {
    if (listIterator != parts.end()) return *vectorIterator;
    else return '\0';
}

PartStream::Ch PartStream::Take() {
    if (listIterator != parts.end()) {
        Ch tmp = *vectorIterator;

        ++vectorIterator;
        ++count;

        if (vectorIterator == listIterator->end()) {
            ++listIterator;
            skipEmptyParts();
        }

        return tmp;
    }
    else {
        return '\0';
    }
}

size_t PartStream::Tell() {
    return count;
}

PartStream::Ch* PartStream::PutBegin() {
    assert(false);
    return nullptr;
}

void PartStream::Put(Ch c) {
    assert(false);
}

void PartStream::Flush() {
    assert(false);
}

size_t PartStream::PutEnd(Ch* begin) {
    assert(false);
    return 0;
}

void PartStream::reset() {
    listIterator = parts.begin();
    skipEmptyParts();
    count = 0;
}

void PartStream::skipEmptyParts() {
    while (listIterator != parts.end() && listIterator->empty()) ++listIterator;
    if (listIterator != parts.end()) vectorIterator = listIterator->begin();
}