#ifndef GDAX_SERVICE_PARTSTREAM_H
#define GDAX_SERVICE_PARTSTREAM_H

#include <list>
#include <vector>

/**
 * Поток символов на основе списка частей строки
 */
class PartStream {

public:

    typedef char Ch;

private:

    /**
     * Ссылка на список частей строки
     */
    const std::list<std::vector<Ch>>& parts;

    /**
     * Итератор для списка
     */
    std::list<std::vector<Ch>>::const_iterator listIterator;

    /**
     * Итератор для вектора
     */
    std::vector<Ch>::const_iterator vectorIterator;

    /**
     * Количиство считанных символов
     */
    size_t count = 0;

public:

    /**
     * Конструктор
     * @param parts ссылка на список частей строки
     */
    explicit PartStream(const std::list<std::vector<Ch>>& parts);

    Ch Peek() const;

    Ch Take();

    size_t Tell();

    Ch* PutBegin();

    void Put(Ch c);

    void Flush();

    size_t PutEnd(Ch* begin);

    /**
     * Возвращает поток к первоначальному состоянию
     */
    void reset();

private:

    /**
     * Пропускает пустые части строки
     */
    void skipEmptyParts();

};

#endif //GDAX_SERVICE_PARTSTREAM_H