#ifndef GDAX_SERVICE_HTTPREADERHANDLER_H
#define GDAX_SERVICE_HTTPREADERHANDLER_H

#include "OrderBook.h"
#include <rapidjson/reader.h>

/**
 * Обработчик событий для разбора HTTP ответа от сервера в формате Json
 */
class HttpReaderHandler : public rapidjson::BaseReaderHandler<rapidjson::UTF8<>, HttpReaderHandler> {

private:

    /**
     * Состояние
     */
    enum State {
        ExpectObjectStart,
        ExpectObjectEnd,
        ExpectSequenceKey,
        ExpectSequenceNumber,
        ExpectBidsKey,
        ExpectBidsArrayStart,
        ExpectOrderArrayStartOrBidsArrayEnd,
        ExpectAsksKey,
        ExpectAsksArrayStart,
        ExpectOrderArrayStartOrAsksArrayEnd,
        ExpectOrderArrayEnd,
        ExpectOrderPriceString,
        ExpectOrderSizeString,
        ExpectOrderIdString
        };

private:

    /**
     * Ссылка на стакан
     */
    OrderBook& orderBook;

    /**
     * Порядковый номер для загруженной копии стакана
     */
    uint64_t sequence = 0;

    /**
     * Переменная для временного хранения цены заявки
     */
    number_type price;

    /**
     * Переменная для временного хранения размера заявки
     */
    number_type size;

    /**
     * Переменная для временного хранения идентификатора заявки
     */
    std::string id;

    /**
     * Переменная для временного хранения типа заявки
     */
    OrderType type;

    /**
     * Состояние
     */
    State state;

public:

    /**
     * Конструктор
     * @param orderBook ссылка на стакан
     */
    explicit HttpReaderHandler(OrderBook& orderBook);

    bool Default();

    bool RawNumber(const Ch* str, rapidjson::SizeType length, bool copy);

    bool String(const Ch* str, rapidjson::SizeType length, bool copy);

    bool StartObject();

    bool Key(const Ch* str, rapidjson::SizeType length, bool copy);

    bool EndObject(rapidjson::SizeType memberCount);

    bool StartArray();

    bool EndArray(rapidjson::SizeType elementCount);

    uint64_t getSequence() const;

};

#endif //GDAX_SERVICE_HTTPREADERHANDLER_H