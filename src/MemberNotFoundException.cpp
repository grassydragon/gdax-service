#include "MemberNotFoundException.h"

MemberNotFoundException::MemberNotFoundException(const char* type, const char* name) {
    message += "Member with type ";
    message += type;
    message += " and name \"";
    message += name;
    message += "\" wasn't found";
}

const char* MemberNotFoundException::what() const noexcept {
    return message.c_str();
}