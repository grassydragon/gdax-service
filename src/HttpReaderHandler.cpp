#include "HttpReaderHandler.h"

using namespace std;

HttpReaderHandler::HttpReaderHandler(OrderBook& orderBook) : orderBook(orderBook) {
    state = ExpectObjectStart;
}

bool HttpReaderHandler::Default() {
    return false;
}

bool HttpReaderHandler::RawNumber(const char* str, rapidjson::SizeType length, bool copy) {
    if (state == ExpectSequenceNumber) {
        string tmp(str, length);
        try {
            sequence = stoul(tmp);
        }
        catch (const exception& e) {
            return false;
        }
        state = ExpectBidsKey;
        return true;
    }

    return false;
}

bool HttpReaderHandler::String(const char* str, rapidjson::SizeType length, bool copy) {
    if (state == ExpectOrderPriceString) {
        price = number_type(string(str, length));
        state = ExpectOrderSizeString;
        return true;
    }
    else if (state == ExpectOrderSizeString) {
        size = number_type(string(str, length));
        state = ExpectOrderIdString;
        return true;
    }
    else if (state == ExpectOrderIdString) {
        id = string(str, length);
        state = ExpectOrderArrayEnd;
        return true;
    }

    return false;
}

bool HttpReaderHandler::StartObject() {
    if (state == ExpectObjectStart) {
        state = ExpectSequenceKey;
        return true;
    }

    return false;
}

bool HttpReaderHandler::Key(const char* str, rapidjson::SizeType length, bool copy) {
    if (state == ExpectSequenceKey && memcmp(str, "sequence", length) == 0) {
        state = ExpectSequenceNumber;
        return true;
    }
    else if (state == ExpectBidsKey && memcmp(str, "bids", length) == 0) {
        state = ExpectBidsArrayStart;
        return true;
    }
    else if (state == ExpectAsksKey && memcmp(str, "asks", length) == 0) {
        state = ExpectAsksArrayStart;
        return true;
    }

    return false;
}

bool HttpReaderHandler::EndObject(rapidjson::SizeType memberCount) {
    return state == ExpectObjectEnd;
}

bool HttpReaderHandler::StartArray() {
    if (state == ExpectBidsArrayStart) {
        type = BID;
        state = ExpectOrderArrayStartOrBidsArrayEnd;
        return true;
    }
    else if (state == ExpectAsksArrayStart) {
        type = ASK;
        state = ExpectOrderArrayStartOrAsksArrayEnd;
        return true;
    }
    else if (state == ExpectOrderArrayStartOrBidsArrayEnd || state == ExpectOrderArrayStartOrAsksArrayEnd) {
        state = ExpectOrderPriceString;
        return true;
    }
    
    return false;
}

bool HttpReaderHandler::EndArray(rapidjson::SizeType elementCount) {
    if (state == ExpectOrderArrayStartOrBidsArrayEnd) {
        state = ExpectAsksKey;
        return true;
    }
    else if(state == ExpectOrderArrayStartOrAsksArrayEnd) {
        state = ExpectObjectEnd;
        return true;
    }
    else if (state == ExpectOrderArrayEnd) {
        orderBook.addOrder(price, size, id, type);
        if (type == BID) state = ExpectOrderArrayStartOrBidsArrayEnd;
        else state = ExpectOrderArrayStartOrAsksArrayEnd;
        return true;
    }
       
    return false;
}

uint64_t HttpReaderHandler::getSequence() const {
    return sequence;
}